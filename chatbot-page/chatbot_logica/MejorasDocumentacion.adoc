= Mejora del Chatbot: ChatBot-AI
:toc:
:toc-title: Contenido
:icons: font
:experimental:
:leveloffset: 0.


== OBJETIVOS DE LAS MEJORAS

** Expansión de dialogo
** Agregación de conectores
** Mejora de coencidencia de palabra
** Implementacion de falta de ortografia
** Enfoque mas interactivo

== EXPANSIÓN DE DIALOGO

** Inicialmente nuestro chatbot solo se limitaba en responder a palabras que esten relacionado con el `saludo`, `despedida` ,`agradecimiento`, una vez teniendo los pilares del chatbot empezamos a expandir su conocimiento de palabras, para incluir información relacionada con los servicios ofrecidos por el Hospital Dr. Larcade. Esta expansión del conocimiento ha permitido que el chatbot proporcione orientación y detalles específicos sobre los servicios disponibles en el hospital, actuando como una guía para los usuarios interesados en obtener información relacionada con la atención médica que ofrece el centro.

** Al incluir nuevas palabras y conceptos relacionados al hospital Dr.Larcade, se enriquesio la capacidad de respuesta del chatbot, permitiéndole comprender y responder a una gama más amplia de consultas y preguntas relacionadas con la atención médica. Esto mejora significativamente la utilidad y la experiencia del usuario, ya que pueden obtener información precisa y relevante sobre los servicios médicos disponibles en el  de una manera rápida y accesible.

** Actualmente las etiquetas que contiene nuestro intenciones.json son:

***** `despedida`
***** `Saludo`
***** `agredecimiento`
*****  etapa1-2_informaciongenel
*****  etapa1-2_numeros
***** etapa3-4_telefono_hospital
***** etapa3-4_telefono_SAME
***** etapa3-4_telefono_cardiologia
***** etapa3-4_telefono_turnos
***** etapa1-2_servicio
***** etapa1-2_estudios
***** estudios: estudios_ergometriaestudios_presurometria,estudios_ecocardiograma,estudios_electrocardiograma,estudios_tomografias_computadas,estudios_tomografias_multislice,estudios_radiologia_placas_simples,
estudios_contrastados,
estudios_seriada_esofagogastroduodenal,
estudios_colangiografia,
estudios_urograma_extretor,
estudios_ecografias,
estudios_eco_doppler,
estudios_laboratorios_completos
***** nv-2-otro-tipo-de-consulta
***** nv-4-otro-tipo-de-consulta-publico
***** nv-3-otro-tipo-de-urgencia
***** urgencia_dolor
***** etapa1-2_capacitacion
***** etapa3-4_capacitacion_educacion
***** etapa3-4_capacitacion_formacion
***** etapa3-4_charla
***** etapa3-4_cursos
***** servicios: servicio_ginecologia_adolescente, servicio_ginecologia_general, servicio_hematologia, servicio_infectologia, servicio_infectologia_infantil, servicio_maternidad, servicio_nefrologia, servicio_neurocirugia, servicio_neurologia, servicio_nutricion, servicio_otorrinolaringologia, servicio_odontologia, servicio_oftalmologia, servicio_oncologia, servicio_oncologia_infantil, servicio_ortopedia, servicio_ortodoncia, servicio_otorrinolaringologia_infantil, servicio_pediatra, servicio_pediatria_adulto, servicio_pediatria_adolescente, servicio_pediatria_general, servicio_psicologia, servicio_psiquiatria, servicio_radiologia, servicio_reumatologia, servicio_terapia_intensiva, servicio_traumatologia, servicio_uco, servicio_urgencia_adulto, servicio_urgencia_infantil, servicio_urologia, servicio_vias_respiratorias_infantil, servicio_respiratorio, servicio_Dermatologia, servicio_cirugia, servicio_cardiologia, servicio_cardiologia_infantil, servicio_cardiologia_riesgo_quirurgico, servicio_cirugia_cuello_cabeza, servicio_cirugia_traumatologica_rodilla, servicio_cirugia_general, servicio_cirugia_plastica, servicio_cirugia_vascular, servicio_clinica_medica, servicio_curaciones, servicio_dermatologia_general, servicio_dermatologia_infantil, servicio_diabetes, servicio_eco_cardiogramas, servicio_ecocardio_dopler, servicio_ecodopler_periferico, servicio_endocrinologia, servicio_ergometrias, servicio_espirometros, servicio_fonoaudiologia, servicio_gastroenterologia




 

== AGREGACIÓN DE CONECTORES

** En el trascurso del proyecto del chatbot, se presentó un problema relacionado con los conectores, en nuestro `intension.json` contiene las palabras claves pero no tiene todos los conectores que existe en español, para solucionar este problema, creamos un json llamado `conectores.json`, ya que si el usuario ingresa cierta oración con conectores que no está en nuestro `intension.json` entonces `conectores.json` se utilizara como soporte, esto nos sirve para que nuestro chatbot pueda encontrar las palabras clave y que haya una probabilidad más elevada. si el usuario ingresa una palabra que no es ni un conector y no es una palabra clave para el chatbot, entonces se baja la probabilidad

image::imagen/mejorachatbot1.JPG[]


== MEJORA DE COENCIDENCIA DE PALABRA

** Inicialmente, nuestro chatbot enfrentaba un inconveniente en el que, al recibir una frase extensa por parte del usuario, solo reconocía una palabra clave y respondía basándose únicamente en esa palabra, ignorando el resto del contenido de la oración. Esto podía generar situaciones en las que la respuesta del chatbot no se alineaba con el contexto completo de la frase del usuario, lo que a menudo resultaba en interacciones confusas o incoherentes. Incluso si el resto de la oración del usuario no tenía sentido, si una palabra era reconocida por el chatbot, este proporcionaba una respuesta basada únicamente en esa palabra clave. Esta limitación afectaba la capacidad del chatbot para comprender y responder adecuadamente a las consultas de los usuarios, ya que ignoraba información relevante contenida en la oración completa. Como resultado, las respuestas del chatbot podían ser inadecuadas o poco útiles para los usuarios, lo que afectaba negativamente la calidad de la experiencia del usuario.

** Como solucion se implemento otro nuevo algoritmo como soporte, no se remplazo el algoritmo origial `bolsa_de_palabra/bag of bag of words`:

image::imagen/mejorachatbot0.JPG[]

** Se implemento un nuevo algoritmo que se llama `probabilidad_de_coincidencia_de_patron` en donde se toma la cantidad de elementos y se va sumando 1 punto por cada palabra que conoce mi chatbot, es decir, si tenemos la frase `Hola, don quijote de la mancha` seran 6 palabras y si mi chatbot solo conoce las palabras [`hola, servicios,ubicacion, cardilogia, horario, turnos,contacto`] entonces la unica coencidencia sera la palabra `hola` entonces la totalidad de punto es 1 de 6, dicho de otra manera, la probabilidad es de 1/6=0.166..7 lo que singifica que la probabilidad es baja y el chatbot no comprende lo que esta diciendo el usuario. 

image::imagen/mejorachatbot.JPG[]


== MODIFICACIONES DEL CHATBOT

=== Enfoque mas interactivo:

** Al principio el chatbot era mas como una consulta mediante comandos, es decir, si el usuario ingrasaba la palabra `servicios` el chatbot te daba todo un listado completo de los servicios que ofrecen el hospital, el chatbot se modifico para que se mas interactivo con el usuario. Ahora, el chatbot genera preguntas específicas para ayudar al usuario a identificar qué información necesita o para mostrarle información relevante que pueda interesarle.

** La interacción con el usuario se puede dividir en cuatro etapas. La primera etapa ocurre cuando el usuario hace una pregunta genérica, como `¿Qué información me puedes dar?`. En esta etapa, el chatbot proporciona un listado de los servicios ofrecidos por el hospital. La segunda etapa se activa cuando el chatbot responde a la pregunta del usuario. Dentro de este listado, el usuario puede encontrar información que le interese particularmente. Cuando el usuario pregunta específicamente por esta información, pasamos a la tercera etapa. En esta etapa, el chatbot responde a la pregunta específica del usuario. Una vez que el chatbot ha respondido a la pregunta puntual del usuario, se completa la cuarta etapa de la interacción.


image::imagen/mejorachatbot2.JPG[]

** Esta nueva función mejora la experiencia del usuario al proporcionar respuestas más personalizadas y facilitar la navegación por la amplia gama de servicios disponibles en el hospital




== LO QUE NO SE LLEGO IMPLEMENTAR

=== Mas de una peticion
** Actualmente el chatbot puede brindarte una respuesta centrada de una consulta, pero si el usuario ingresa doble petición, por ejemplo: `dame el listado de servicios y la ubicación del hospital` solo responderá una sola petición, no brindará dos información al mismo tiempo.



== DIFICULTADES

** Palabras claves: El propósito del chatbot es brindar una guía al usuario para informarle que tipo de servicios y estudios se puede realizar en el hospital DR.larcade, el tema es cuando el usuario ingresa ciertas palabras de solicitud pidiendo información de servicios o estudios, la forma de pedir información puede causar problema en el entendimiento del chatbot porque puede confundirse con servicios o estudios.

** Falta de ortografía: Nuestro chatbot está diseñado para reconocer y comprender las palabras bien escrita, es decir, palabras sin falta de ortografía, el problema se origina cuando los usuarios ingresan palabras con errores ortográficos, lo que puede afectar su capacidad de respuesta o prácticamente no entender la respuesta.