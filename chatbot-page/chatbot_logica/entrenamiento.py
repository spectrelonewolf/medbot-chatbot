import numpy as np
import random
import json

import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from unidecode import unidecode
from nltk_utils import bolsa_de_palabra, tokenizar, raiz
from modelo import RedNeuronal

with open('intenciones.json', 'r', encoding='utf-8') as f:
    intenciones = json.load(f)

unidecode(json.dumps(intenciones))
print(intenciones)
toda_las_palabra = []
tags = []
xy = []

# Recorrer cada oración en nuestras intenciones de patrones
for intencion in intenciones['intents']:
    tag = intencion['tag']
     # Añadir a la lista de etiquetas
    tags.append(tag)
    for patron in intencion['patron']:
        # Tokenizar cada palabra de la oración
        w = tokenizar(patron)
          # Añadir a nuestra lista de palabras
        toda_las_palabra.extend(w)
         # Añadir al par xy
        xy.append((w, tag))

# Extraer la raíz y convertir a minúsculas cada palabra
ignorar_palabras = ['?','¿' '.', '!','Â']
toda_las_palabra = [raiz(w) for w in toda_las_palabra if w not in ignorar_palabras]
# Eliminar duplicados y ordenar
toda_las_palabra = sorted(set(toda_las_palabra))
tags = sorted(set(tags))

print(len(xy), "patron")
print(len(tags), "tags:", tags)
print(len(toda_las_palabra), "Palabras de raíz únicas:", toda_las_palabra)

# Crear datos de entrenamiento
X_entrenamiento = []
Y_entrenamiento  = []
for (pattern_sentence, tag) in xy:
     # X: bolsa de palabras para cada patrón de sentencia
    bag = bolsa_de_palabra(pattern_sentence, toda_las_palabra)
    X_entrenamiento.append(bag)
  # y: La función de pérdida de entropía cruzada de PyTorch solo necesita etiquetas de clase, no codificación one-hot
    label = tags.index(tag)
    Y_entrenamiento .append(label)

X_entrenamiento = np.array(X_entrenamiento)
Y_entrenamiento  = np.array(Y_entrenamiento )

# Hiperparámetros
num_epochs = 1000
tamano_lote = 8
tasa_aprendizaje= 0.001
tamano_entrada = len(X_entrenamiento[0])
tamano_oculto = 8
tamano_salida = len(tags)
print(tamano_entrada, tamano_salida)

class ChatDataset(Dataset):

    def __init__(self):
        self.n_samples = len(X_entrenamiento)
        self.x_data = X_entrenamiento
        self.y_data = Y_entrenamiento 

   # Admite la indexación de modo que dataset[i] se pueda usar para obtener la i-ésima muestra
    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index]

    # Podemos llamar a len(dataset) para devolver el tamaño
    def __len__(self):
        return self.n_samples

conjunto_datos = ChatDataset()
carga_de_entrenamiento  = DataLoader(dataset=conjunto_datos,
                          batch_size=tamano_lote,
                          shuffle=True,
                          num_workers=0)

dispositivo = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

modelo = RedNeuronal(tamano_entrada, tamano_oculto, tamano_salida).to(dispositivo)

#  Pérdida y optimizacion
criterio = nn.CrossEntropyLoss()
optimizador = torch.optim.Adam(modelo.parameters(), lr=tasa_aprendizaje )


# Entrenamiento del modelo
for epoch in range(num_epochs):
    for (palabra, etiqueta) in carga_de_entrenamiento :
        palabra = palabra.to(dispositivo)
        etiqueta = etiqueta.to(dtype=torch.long).to(dispositivo)
        
         # Pase hacia adelante
        salida = modelo(palabra)
      # Si y fuera one-hot, debemos aplicar etiquetas = torch.max(etiquetas, 1)[1]
        # labels = torch.max(labels, 1)[1]
        perdida = criterio(salida, etiqueta)
        
         # Retroceder y optimizar
        optimizador.zero_grad()
        perdida.backward()
        optimizador.step()
        
    if (epoch+1) % 100 == 0:
        print (f'iteracion [{epoch+1}/{num_epochs}], perdida: {perdida.item():.4f}')


print(f'perdida final: {perdida.item():.4f}')

data = {
"model_state": modelo.state_dict(),
"input_size": tamano_entrada,
"hidden_size": tamano_oculto,
"output_size": tamano_salida,
"all_words": toda_las_palabra,
"tags": tags
}

FILE = "data.pth"
torch.save(data, FILE)

print(f'Entrenamiento completado. Archivo guardado en {FILE}')

