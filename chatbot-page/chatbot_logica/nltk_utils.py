import nltk
import numpy as np

nltk.download('punkt')
nltk.download('words')

from nltk.stem import SnowballStemmer
stemmer = SnowballStemmer('spanish')

def tokenizar(sentencia):
    return nltk.word_tokenize(sentencia)

def raiz(palabra):
    return stemmer.stem(palabra.lower())

def bolsa_de_palabra(sentencia_tokenizada, todas_las_palabras):

    sentencia_tokenizada = [raiz(w) for w in sentencia_tokenizada]
 
    # Se inicializa una bolsa de palabras con ceros
    bolsa = np.zeros(len(todas_las_palabras), dtype=np.float32)
    # Se marcan las demás palabras presentes en la bolsa de palabras
    for idx, w in enumerate(todas_las_palabras):
        if w in sentencia_tokenizada:
            bolsa[idx] = 1.0

    return bolsa

sentencias = ["Hola", "Ey", "como estas", "Hay alguien ahi", "Hola", "Buen dia"]
palabras = ["hola", "ey", "como estas", "buen dia"]
vector='[1. 1. 1. 0. 0. .1]'
vector = bolsa_de_palabra(sentencias, palabras)

#ejemplo
#a = "Hola, ¿qué puedo hsacer por ti?"
#a = tokenizar(a)
#print(a)

#ejemplo de racies de palabras
#palabras_ejemplo = ["Hola", "buenos", "días", "tardes","ayudadamente","ayudin"]
#raices_palabra = [raiz(w) for w in palabras_ejemplo]
#raices_palabra_utilizando_pytorch = [raiz(w) for w in palabras_ejemplo]
#print(raices_palabra)


#la idea principal es se realiza una division por la cantidad de palabra que aprezca en la oracion con la cantidad de patron que existe. 
def probabilidad_de_coincidencia_de_patron(mensaje, patrones_conocidos,conectore):
    raices_palabras_mensaje = [raiz(palabra) for palabra in mensaje]
    total_palabras_mensaje = len(raices_palabras_mensaje)
    coincidencias = sum(1 for palabra in raices_palabras_mensaje if palabra in patrones_conocidos or palabra in conectore)
    probabilidad = coincidencias / total_palabras_mensaje if total_palabras_mensaje > 0 else 0

    return probabilidad

# Ejemplo de probabilidad
#patrones_conocidos = {"Hola", "buenos", "días", "tardes","ayudadamente","ayudin"}
#conectores = {"la", "y", "el", "por","de"}
#mensaje = "hola principio del palomar"
#probabilidad = probabilidad_de_coincidencia_de_patron(mensaje, patrones_conocidos,conectores)
#print("Probabilidad del mensaje:", probabilidad)


def corregir_ortografia(oracion, ortografia):

    caracteres_eliminar = '¿?,!¡()#.'
    
    # Elimina los caracteres de la cadena de texto
    oracion_limpia = ''.join(c for c in oracion if c not in caracteres_eliminar)
    
    palabras = oracion_limpia.split()
    oracion_corregida = []
    
    # Itera sobre cada palabra de la oración
    for palabra in palabras:
        # Variable para almacenar la palabra corregida
        palabra_corregida = palabra
        
        # Itera sobre cada grupo de palabras mal escritas en el JSON de ortografía
        for grupo_palabras in ortografia['palabras']:
            palabras_mal_escritas = grupo_palabras['mal-escrita']
            palabras_bien_escritas = grupo_palabras['bien-escrita']
            # Verifica si la palabra actual coincide con alguna palabra mal escrita
            if palabra in palabras_mal_escritas:
                 palabra_corregida = palabras_bien_escritas[0]
                 oracion = oracion.replace(palabra, palabra_corregida)
        
        # Agrega la palabra corregida a la lista de palabras corregidas
        oracion_corregida.append(palabra_corregida)
    
    # Une las palabras corregidas en una oración nuevamente
    return ' '.join(oracion_corregida)
