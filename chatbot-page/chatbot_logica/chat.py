import random
import json
import torch
from .modelo import RedNeuronal
from unidecode import unidecode
from .nltk_utils import bolsa_de_palabra, tokenizar,probabilidad_de_coincidencia_de_patron,raiz,corregir_ortografia

import os

dipositivo = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Obtiene la ruta del directorio actual
current_directory = os.path.dirname(os.path.abspath(__file__))
current_directory2 = os.path.dirname(os.path.abspath(__file__))
current_directory3 = os.path.dirname(os.path.abspath(__file__))


# Une la ruta del archivo intenciones.json y conectores.json con lo directoriores actuales
file_path = os.path.join(current_directory, 'intenciones.json')
file_path2 = os.path.join(current_directory2, 'conectores.json')
file_path3 = os.path.join(current_directory2, 'ortografia.json')

# Abre el archivo usando la ruta completa
with open(file_path, 'r', encoding='utf-8') as json_data:
    intenciones = json.load(json_data)

with open(file_path2, 'r', encoding='utf-8') as json_data:
    conectores = json.load(json_data)


with open(file_path3, 'r', encoding='utf-8') as json_data:
    ortografia = json.load(json_data)


# Une la ruta del archivo data.pth con el directorio actual
file_path = os.path.join(current_directory, 'data.pth')

#obtengo la informacion de conectores.json
conectore=[]
for conector in conectores["conectores"]:
    w = tokenizar(conector)
    conectore.extend(w)

conectore= [raiz(w) for w in conectore] 
# Eliminar duplicados y ordenar
conectore = sorted(set(conectore))




# Abre el archivo (data.pth) usando la ruta completa
with open(file_path, 'rb') as f:
    data = torch.load(f)


tamano_entrada = data["input_size"]
tamano_oculto = data["hidden_size"]
tamano_salida = data["output_size"]
todas_la_palabra = data['all_words']
tags = data['tags']
estado_del_modelo = data["model_state"]

model = RedNeuronal(tamano_entrada, tamano_oculto, tamano_salida).to(dipositivo)
model.load_state_dict(estado_del_modelo)
model.eval()

nombre_del_bot = "MEDBot"

def chat(message):
    response= ""

    message=unidecode(message) 
    oracion = message 
    oracion = message.lower()

    oracion =corregir_ortografia(oracion, ortografia)
    oracion = tokenizar(oracion)
    X = bolsa_de_palabra(oracion, todas_la_palabra)
    X = X.reshape(1, X.shape[0])
    X = torch.from_numpy(X).to(dipositivo)
    output = model(X)
    _, predicted = torch.max(output, dim=1)
    tag = tags[predicted.item()]
    probs = torch.softmax(output, dim=1)
    probabilidad = probs[0][predicted.item()]
    probabilidad_mensaje = probabilidad_de_coincidencia_de_patron(oracion, todas_la_palabra,conectore)
    if probabilidad.item() > 0.63 and probabilidad_mensaje > 0.5:
        for intencion in intenciones['intents']:
            if tag == intencion["tag"]:
                 response = f"{nombre_del_bot}: {random.choice(intencion['respuesta'])}"
    else:
        response= f"{nombre_del_bot}: Lo siento pero No comprendo..."
    return response