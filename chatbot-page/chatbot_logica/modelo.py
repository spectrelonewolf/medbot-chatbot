import torch
import torch.nn as nn


class RedNeuronal(nn.Module):
    def __init__(self, tamano_entrada, tamano_oculto, num_clases):
        super(RedNeuronal, self).__init__()
        self.l1 = nn.Linear(tamano_entrada, tamano_oculto) 
        self.l2 = nn.Linear(tamano_oculto, tamano_oculto) 
        self.l3 = nn.Linear(tamano_oculto, num_clases)
        self.relu = nn.ReLU()
    
    def forward(self, x):
        out = self.l1(x)
        out = self.relu(out)
        out = self.l2(out)
        out = self.relu(out)
        out = self.l3(out)
        # no hay activación ni softmax al final
        return out
