
################################################################IMPORTS#########################################3
#############################################################IMPORTS####################################################3
import secrets
import json
import base64
import os

from flask import Flask, send_from_directory, request, jsonify, session, render_template, redirect, url_for
from chatbot_logica.chat import chat
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime 
from sqlalchemy import desc
from operator import attrgetter
from flask_socketio import SocketIO, send
from webauthn import generate_registration_options, options_to_json, verify_registration_response, generate_authentication_options, verify_authentication_response, base64url_to_bytes
from webauthn.helpers.structs import (
    AttestationConveyancePreference,
    AuthenticatorAttachment,
    AuthenticatorSelectionCriteria,
    PublicKeyCredentialDescriptor,
    PublicKeyCredentialType,
    AuthenticatorTransport,
    ResidentKeyRequirement
)
#################################################################################################################################
#Definicion de la carpeta static para carga de la pagina reutilizada
app = Flask(__name__, static_folder='static', static_url_path='')
########################################################### BASE DE DATOS #########################################################
########################################################### BASE DE DATOS #########################################################
# Coonfiguracion de la base de datos dentro de Flask
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///chat.db'
app.config['SECRET_KEY'] = '12344321'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Creación de la tabla dentro de la base de datos
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email =  db.Column(db.String(45), unique=True, nullable=False)
    user_id = db.Column(db.LargeBinary, unique=True, nullable=False)
    credential_id = db.Column(db.LargeBinary, unique=True)
    public_key = db.Column(db.LargeBinary, unique=True)
    sign_count = db.Column(db.Integer)


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_message = db.Column(db.Text, nullable=True)
    bot_response = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return f"Chat('{self.timestamp}', '{self.user_id}', '{self.user_message}', '{self.bot_response}')"

######################################################### TEST TABLAS #######################################################
# Ruta para mostrar los chats asociados a un correo electrónico
@app.route('/chats/<string:email>')
def show_chats(email):
    # Realizar un join entre las tablas User y Chat para obtener los chats asociados al correo electrónico proporcionado
    user_chats = db.session.query(User, Chat).join(Chat).filter(User.email == email).all()

    # Verificar si se encontraron resultados
    if user_chats:
        return render_template('chats.html', user_chats=user_chats)
    else:
        return 'No se encontraron chats para el usuario con el correo electrónico proporcionado'

@app.route('/ver_contenido')
def ver_contenido():
    # Obtener todos los usuarios y todos los chats
    usuarios = User.query.all()
    chats = Chat.query.all()

    return render_template('ver_contenido.html', usuarios=usuarios, chats=chats)

# Ruta para eliminar todos los valores de las tablas
@app.route('/eliminar_valores')
def eliminar_valores():
    # Eliminar todos los registros de la tabla User
    db.session.query(User).delete()
    # Eliminar todos los registros de la tabla Chat
    db.session.query(Chat).delete()
    # Confirmar los cambios
    db.session.commit()
    # Redirigir a la página de verificación exitosa
    # Obtener todos los usuarios y todos los chats
    usuarios = User.query.all()
    chats = Chat.query.all()

    return render_template('ver_contenido.html', usuarios=usuarios, chats=chats)

@app.route('/user/chats/<email>', methods=['GET'])
def user_chats(email):
    return get_user_chats(email)


def get_user_chats(email):
    # Buscar el usuario por su correo electrónico
    user = User.query.filter_by(email=email).first()

    # Verificar si el usuario existe
    if user:
        # Obtener todos los chats asociados al usuario
        user_chats = Chat.query.filter_by(user_id=user.id).all()

        # Crear una lista de diccionarios con los detalles de cada chat
        chat_list = []
        for chat in user_chats:
            chat_data = {
                'timestamp': chat.timestamp,
                'user_id': chat.user_id,
                'user_message': chat.user_message,
                'bot_response': chat.bot_response
            }
            chat_list.append(chat_data)

        return jsonify(chat_list), 200
    else:
        # Devolver un mensaje de error si el usuario no existe
        return jsonify({'error': 'Usuario no encontrado'}), 404

#######################################################ADMINISTRACION DE CUENTA################################################################
#################################################################################################################################
#DOMINIO sin https y sin //
RP_ID='hospital-larcade.myddns.me' 
#######################################################ADMINISTRACION DE CUENTA################################################################
@app.route("/user_id", methods=["GET"])
def get_user_id():
    user_id = session.get('user_id')
    return jsonify(user_id=user_id)

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('/'))

def generate_challenge():
    return os.urandom(32)

# Ruta para registrar un nuevo usuario
@app.route('/verificar_registro', methods=['POST'])
def verificar_registro():
    email = session.get('user_name')
    client_response = request.json.get('response')
    inner_response = client_response.get('response')
    client_data_json = inner_response.get('clientDataJSON')
    attestation_object = inner_response.get('attestationObject')

    client_data_json_b64 = client_response.get('response').get('clientDataJSON')
    client_data_json_str = base64.urlsafe_b64decode(client_data_json_b64 + '==').decode()
    client_data_json_obj = json.loads(client_data_json_str)

    expected_challenge = base64url_to_bytes(client_data_json_obj.get('challenge'))
    expected_origin = client_data_json_obj.get('origin')

    credential = {
        'id': client_response.get('id'),
        'rawId': client_response.get('rawId'),
        'response': {
            'clientDataJSON': client_data_json,
            'attestationObject': attestation_object,
        },
        'type': "public-key"
    }
    
    webauthn_response = verify_registration_response(
        credential=credential,
        expected_challenge=expected_challenge,
        expected_rp_id=RP_ID,
        expected_origin=expected_origin,
        require_user_verification=True
    )

    # Almacenar el usuario en la base de datos
    ukey_bytes = os.urandom(15)
    new_user = User(email=email, user_id = ukey_bytes)
    db.session.add(new_user)
    db.session.commit()
    user_name = session.get('user_name')
    user = User.query.filter_by(email=user_name).first()
    if user is None:
        return jsonify({'status': 'failed', 'message': 'User not found'}), 404

    # Verificar si todos los datos necesarios están presentes
    if not webauthn_response.credential_id or not webauthn_response.credential_public_key:
        # Eliminar al usuario incompleto de la base de datos
        user_name = session.get('user_name')
        user = User.query.filter_by(email=user_name).first()
        if user:
            db.session.delete(user)
            db.session.commit()
        return jsonify({'status': 'failed', 'message': 'Datos incompletos'}), 400

    user.credential_id = webauthn_response.credential_id
    user.public_key = webauthn_response.credential_public_key
    user.sign_count = webauthn_response.sign_count

    db.session.commit()

    return jsonify({'status': 'ok'})

# Ruta para registrar un nuevo usuario
@app.route('/registrar', methods=['POST'])
def registrar():
    email = request.json.get('email')

    existing_user = User.query.filter_by(email=email).first()
    if existing_user:
        return jsonify({'status': 'failed', 'message': 'El correo electrónico ya está registrado'}), 401

    challenge = generate_challenge()

    ukey_bytes = os.urandom(15)
    ukey_base64 = base64.urlsafe_b64encode(ukey_bytes)	
    if not isinstance(ukey_base64, str):
        ukey_base64 = ukey_base64.decode('utf-8')

    # No almacenar el usuario aquí, sino en la verificación exitosa
    session['user_name'] = email

    make_credential_options = generate_registration_options(
        user_id=ukey_bytes,
        user_name=email,
        attestation=AttestationConveyancePreference.DIRECT,
        authenticator_selection=AuthenticatorSelectionCriteria(
            authenticator_attachment=AuthenticatorAttachment.PLATFORM,
            resident_key=ResidentKeyRequirement.PREFERRED,
        ),
        challenge=challenge,
        timeout=120000,
        rp_id=RP_ID,
        rp_name="Hospital-Larcade",
    )

    return options_to_json(options=make_credential_options), 200

# Ruta para loguear un usuario
@app.route('/iniciar_sesion', methods=['POST'])
def iniciar_sesion():
    data = request.get_json()
    email = data.get('email')

    user = User.query.filter_by(email=email).first()
    session['user_id'] = user.id
    
    if not user:
         return jsonify({'message': 'Credenciales inválidas'}), 401

    challenge = generate_challenge()


    if user:
            allow_credentials = [PublicKeyCredentialDescriptor(
                id=user.credential_id,
                type=PublicKeyCredentialType.PUBLIC_KEY,
                transports=[AuthenticatorTransport.USB, AuthenticatorTransport.NFC, 
                            AuthenticatorTransport.BLE, AuthenticatorTransport.INTERNAL,
                            AuthenticatorTransport.HYBRID],
            )]
            options = generate_authentication_options(
                    rp_id=RP_ID,
                    challenge=challenge,  
                    timeout=60000,
                    allow_credentials=allow_credentials,
                )
            return options_to_json(options=options), 200
    else:
        return jsonify({'message': 'Credenciales inválidas'}), 401

# Ruta para loguear un usuario
@app.route('/verifiar_inicio_sesion', methods=['POST'])
def verifiar_inicio_sesion():
    client_response = request.json.get('response')  
    client_data_json_b64 = client_response.get('response').get('clientDataJSON')
    client_data_json_str = base64.urlsafe_b64decode(client_data_json_b64 + '==').decode()

    client_data_json_obj = json.loads(client_data_json_str)

    expected_challenge = base64url_to_bytes(client_data_json_obj.get('challenge'))
    expected_origin = client_data_json_obj.get('origin')

  
    credential = {
        'id': client_response.get('id'),
        'rawId': client_response.get('rawId'),
        'response': {
            'clientDataJSON': client_response.get('response').get('clientDataJSON'),
            'authenticatorData': client_response.get('response').get('authenticatorData'),
            'signature': client_response.get('response').get('signature'),
            'userHandle': client_response.get('response').get('userHandle'),
        },
        'type': "public-key"
    }

    user_name = session.get('user_name')

    user = User.query.filter_by(email=user_name).first()

    if user is None:
        return jsonify({'status': 'failed', 'message': 'User not found'})

    webauthn_response = verify_authentication_response(
        credential=credential,
        expected_challenge=expected_challenge,
        expected_rp_id=RP_ID,
        expected_origin=expected_origin,
        credential_public_key=user.public_key,
        credential_current_sign_count=user.sign_count,
        require_user_verification=False
    )

    user.sign_count = webauthn_response.new_sign_count
    db.session.commit()

    return jsonify({'status': 'ok'})

########################################################### RUTEO/VISTAS #########################################################
########################################################### RUTEO/VISTAS #########################################################
#Inicio de la vista del usuario dentro del sitio
@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

#Vista/Ruteo para que el usuario pueda enviar mensajes al chatbot.
@app.route('/chat/', methods=['POST'])
def handle_chat():
    data = request.json
    message = data['message']
    response = chat(message)
    return jsonify({'response': response})

@app.route('/chat/save/', methods=['POST'])
def save_message():
    data = request.get_json()
    user_message = data.get('usuario')
    bot_response = data.get('MEDBot')
    
    # Obtener el ID de usuario de los datos proporcionados
    user_id = data.get('user_id')

    # Verificar si se proporcionó un ID de usuario
    if user_id is None:
        return jsonify({'error': 'No se proporcionó el ID del usuario'}), 400
    
    # Buscar el usuario por su ID
    user = User.query.get(user_id)
    
    # Si el usuario no existe, crear uno nuevo
    if not user:
        # Crear un nuevo usuario con el ID proporcionado
        user = User(id=user_id)
        db.session.add(user)
        db.session.commit()
    
    # Guardar el mensaje del usuario y la respuesta del bot en la base de datos
    new_chat = Chat(user_id=user.id, user_message=user_message, bot_response=bot_response)
    db.session.add(new_chat)
    db.session.commit()

    return jsonify({'success': True}), 200

# Obtener el historial de chat en orden inverso
@app.route('/chat/history/', methods=['GET'])
def chat_history():
    # Obtener el ID de usuario de los parámetros de la URL
    user_id = request.args.get('user_id')

    # Verificar si se proporcionó un ID de usuario
    if user_id is None:
        return jsonify({'error': 'No se proporcionó ningún ID de usuario'}), 400

    # Buscar al usuario por su ID
    user = User.query.get(user_id)

    # Verificar si el usuario existe
    if user:
        # Obtener todos los chats asociados al usuario y ordenarlos por fecha descendente
        user_chats = Chat.query.filter_by(user_id=user.id).order_by(Chat.timestamp.desc()).all()

        # Crear una lista de diccionarios con los detalles de cada chat
        chat_history = []
        for chat in user_chats:
            chat_data = {
                'timestamp': chat.timestamp,
                'user_message': chat.user_message,
                'bot_response': chat.bot_response
            }
            chat_history.append(chat_data)

        # Devolver el historial de chat en formato JSON
        return jsonify(chat_history), 200
    else:
        # Devolver un mensaje de error si el usuario no existe
        return jsonify({'error': 'Usuario no encontrado'}), 404


########################################################### LANZAMIENTO DE LA APP #########################################################
if __name__ == '__main__':
    app.run(debug=True)
########################################################### LANZAMIENTO DE LA APP #########################################################

