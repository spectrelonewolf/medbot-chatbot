import { initializeApp } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-app.js";
import {
  getAuth,
  onAuthStateChanged,
  signInWithEmailAndPassword,
} from "https://www.gstatic.com/firebasejs/10.1.0/firebase-auth.js";
import { getFirestore } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-firestore.js";
import {
  getDatabase,
  ref,
  set,
  child,
  get,
} from "https://www.gstatic.com/firebasejs/10.1.0/firebase-database.js";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

//Crear cuenta en Firebase//////////////////////////////////////////////////

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
//Firebase ////////////////////////////////////////////////////////////////
const firebaseConfig = {
  apiKey: "AIzaSyB0KQgbqajXPVmwSjNc_D7K8SSFXbquHPM",
  authDomain: "hosplarcade.firebaseapp.com",
  projectId: "hosplarcade",
  storageBucket: "hosplarcade.appspot.com",
  messagingSenderId: "542588355477",
  appId: "1:542588355477:web:669ef0ad1fe0e6beffac9c",
  measurementId: "G-7XP1JN7RM6",
  databaseURL: "https://hosplarcade-default-rtdb.firebaseio.com/",
};

// Initialize Firebase///////////////////////////////////////////////////////////
export const app = initializeApp(firebaseConfig);
export const database = getDatabase();
export const auth = getAuth();
export const db = getFirestore();

var campoCorreo = document.getElementById("correo");
var campoContraseña = document.getElementById("contraseña");
var mensajeAviso = document.getElementById("mensajeAviso");
var alertaSistema = document.getElementById("alertaSistema");
var textoAlertaDeSistema = document.getElementById("textoMensajeDeAlerta");
var correoBoolean = false;
var contraseñaBoolean = false;
var correo;
var contraseña;

/// CORREO ////////////////////////////////////////////////////////////////////////////

campoCorreo.addEventListener("input", function () {
  var valor = campoCorreo.value.trim();
  // Validación del correo electrónico
  validacionCorreo(valor);
});

function validacionCorreo(valor) {
  if (!/^[\w.-]+@[\w.-]+\.\w+$/.test(valor)) {
    correoBoolean = false;
    return false;
  } else {
    correoBoolean = true;
    return true;
  }
}

///CONTRASEÑA//////////////////////////////////////////////////////////////////////////

campoContraseña.addEventListener("input", function () {
  var valor = campoContraseña.value.trim();
  // Validación de la contraseña
  validacionContraseña(valor);
});

function validacionContraseña(valor) {
  if (valor.length < 8 || !/^[a-zA-Z0-9]+$/.test(valor)) {
    contraseñaBoolean = false;
    return false;
  } else {
    contraseñaBoolean = true;
    return true;
  }
}

///VALIDACION DEL FORMULARIO////////////////////////////////////////////////////
///LOGUEO EN FIREBASE///////////////////////////////////////////////////////////////////////////////////////////////

// Acción cuando se envía el formulario de registro
document
  .getElementById("formulario")
  .addEventListener("submit", function (event) {
    event.preventDefault(); // Evitar el envío del formulario
    //Valida y registra
    validacionDeCampos();
  });

function validacionDeCampos() {
  if (correoBoolean && contraseñaBoolean) {
    mostrarAlerta(
      mensajeAviso,
      "Todo correcto!, enviando solicitud",
      "alert-success"
    );
    cargarCampos();
    ingresoUsuario();
    return true;
  } else {
    mostrarAlerta(
      mensajeAviso,
      "Error: Algunos de los campos esta mal!, cancelando envio de solicitud",
      "alert-danger"
    );
    return false;
  }
}

function cargarCampos() {
  correo = campoCorreo.value;
  contraseña = campoContraseña.value;
}

var user;
var userId;

function ingresoUsuario() {
  signInWithEmailAndPassword(auth, correo, contraseña)
    .then((userCredential) => {
      // Signed in
      user = userCredential.user;
      console.log(user);
      onAuthStateChanged(auth, (user) => {
        if (user) {
          // User is signed in, see docs for a list of available properties
          // https://firebase.google.com/docs/reference/js/auth.user
          userId = user.uid;
          enviarAPagina();
          // ...
        } else {
          // User is signed out
          // ...
        }
      }).catch((error) => {
        const errorCode = error.code;
        console.log("error1:" + errorCode);
        const errorMessage = error.message;
        console.log("error2:" + errorMessage);
        if (errorCode == "auth/user-not-found") {
          console.log("Ingreso a alerta");
          mostrarAlertaSistema();
        }
      });
    })
    .catch((error) => {
      const errorCode = error.code;
      console.log("error3:" + errorCode);
      const errorMessage = error.message;
      console.log("error4:" + errorMessage);
      if (errorCode == "auth/user-not-found") {
        console.log("Ingreso a alerta");
        mostrarAlertaSistema("cuentaNoEncontrada");
      }
      if (errorCode == "auth/wrong-password") {
        console.log("Ingreso a alerta");
        mostrarAlertaSistema("passwordIncorrecto");
      }
    });
}

function enviarAPagina() {
  window.location.href = "paciente.html";
}

//Alertas de sistema////////////////////////////////////////////////////////////

function mostrarAlertaSistema(mensajeDeError) {
  if (mensajeDeError == "passwordIncorrecto") {
    textoAlertaDeSistema.textContent =
      "La contraseña ingresada no es correcta, por favor intentalo de nuevo.";
  } else if (mensajeDeError == "cuentaNoEncontrada") {
    textoAlertaDeSistema.innerHTML =
      "No se encontro el correo electronico en nuestro sistema, revisa si esta bien escrito o <a class='link-primary link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover'>REGISTRATE.</a>";
  }
  cargarToast();
}

function cargarToast() {
  var toastBootstrap = bootstrap.Toast.getOrCreateInstance(alertaSistema);
  toastBootstrap.show();

  // Calcular la posición vertical para centrar el toast con un margen superior
  var windowHeight = window.innerHeight;
  var toastHeight = alertaSistema.offsetHeight;
  var marginTop = (windowHeight - toastHeight) / 2;
  if (marginTop > 20) {
    // Asegurarse de que haya un margen mínimo de 20px
    alertaSistema.style.marginTop = marginTop + "px";
  }

  // Desplazar la vista del usuario hasta el toast
  alertaSistema.scrollIntoView({ behavior: "smooth", block: "center" });
}

//Funciones adicionales/////////////////////////////////////////////////////////////////////////////////////////////

function esperarAElemento(idElemento) {
  console.log("inicio funcion esperarAElemento");
  var intervalo = setInterval(function () {
    var elemento = document.getElementById(idElemento);
    if (elemento) {
      console.log("aparecio el elemento");
      clearInterval(intervalo);
      buscarYPonerImagen();
      return true;
    }
  }, 100); // Comprobar cada 100ms
}

function checkComas(text, numCommas) {
  var arr = text.split(",");
  return arr.length - 1 === numCommas;
}

function esNumero(str) {
  return !isNaN(str);
}

var alertTimers = new Map(); // Usamos un Map en lugar de un objeto

function mostrarAlerta(campo, mensaje, clase) {
  var divAlerta = campo.parentElement.querySelector(".alert");
  if (divAlerta) {
    campo.parentElement.removeChild(divAlerta);
  }

  var divAlertaNueva = document.createElement("div");
  divAlertaNueva.classList.add("alert", clase);
  divAlertaNueva.setAttribute("role", "alert");
  divAlertaNueva.textContent = mensaje;
  campo.parentElement.appendChild(divAlertaNueva);

  // Cancelar temporizador existente si hay uno
  if (alertTimers.has(campo)) {
    clearTimeout(alertTimers.get(campo));
  }

  // Configurar el temporizador para eliminar la alerta después de 2000 ms
  var timer = setTimeout(function () {
    if (campo.parentElement.contains(divAlertaNueva)) {
      campo.parentElement.removeChild(divAlertaNueva);
    }
    alertTimers.delete(campo); // Eliminar la referencia al temporizador
  }, 2000);

  alertTimers.set(campo, timer); // Guardar el temporizador en el Map
}
