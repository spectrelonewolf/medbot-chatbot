document.addEventListener("DOMContentLoaded", function () {
  function credencialesPublicasAJson(pubKeyCred) {
    if (pubKeyCred instanceof Array) {
      let arr = [];
      for (let i of pubKeyCred) arr.push(credencialesPublicasAJson(i));

      return arr;
    }

    if (pubKeyCred instanceof ArrayBuffer) {
      return base64url.encode(
        String.fromCharCode.apply(null, new Uint8Array(pubKeyCred))
      );
    }

    if (pubKeyCred instanceof Object) {
      let obj = {};

      for (let key in pubKeyCred) {
        obj[key] = credencialesPublicasAJson(pubKeyCred[key]);
      }

      return obj;
    }
    return pubKeyCred;
  }

  function urlBase64(base64url) {
    let base64 = base64url.replace(/-/g, "+").replace(/_/g, "/");
    let rawData = window.atob(base64);
    let outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray.buffer;
  }

  const formularioRegistro = document.getElementById("formularioRegistro");
  const emailInputRegistro = formularioRegistro.querySelector("#registerEmail");
  const botonSubmitRegistro = formularioRegistro.querySelector(
    "button[type='submit']"
  );

  formularioRegistro.addEventListener("submit", function (event) {
    event.preventDefault();
    comienzoWebauthn(emailInputRegistro.value);
  });

  botonSubmitRegistro.addEventListener("click", function (event) {
    event.preventDefault();
    comienzoWebauthn(emailInputRegistro.value);
  });

  // Función para registrar un nuevo usuario
  async function comienzoWebauthn(email) {
    try {
      const optionsResponse = await fetch("/registrar", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      const responseData = await optionsResponse.json();

      if (optionsResponse.ok) {
        const options = responseData;
        options.challenge = urlBase64(options.challenge);
        options.user.id = urlBase64(options.user.id);

        const credential = await navigator.credentials.create({
          publicKey: options,
        });

        let sendCredential = credencialesPublicasAJson(credential);

        const verificarRepuesta = await fetch("/verificar_registro", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ response: sendCredential }),
        });

        if (!verificarRepuesta.ok) {
          throw new Error("Error al verificar el registro");
        }
        showAlertSuccess("Registro existoso! Prueba iniciando sesión");
        document.getElementById("formularioRegistro").reset();
      } else {
        showAlertDanger("El correo ya se encuentra registrado.");
      }
    } catch (error) {
      console.log("ERROr :", error.name);
      if (error.name == "NotAllowedError") {
        showAlertDanger(
          "Se canceló la autenticación con el dispositivo, vuelve a intentarlo."
        );
      } else {
        console.error("Error con el servidor:", error);
      }
    }
  }

  var base64url = {
    encode: function (data) {
      var b64 = btoa(data);
      return b64.replace("+", "-").replace("/", "_").replace(/=+$/, "");
    },
    decode: function (data) {
      data += "==".slice(2 - (data.length & 3));
      return atob(data.replace("-", "+").replace("_", "/"));
    },
  };

  const formularioLogin = document.getElementById("formularioLogin");
  const emailInputLogin = formularioLogin.querySelector("input[name='email']");
  const botonSubmitLogin = formularioLogin.querySelector(
    "button[type='submit']"
  );

  formularioLogin.addEventListener("submit", function (event) {
    event.preventDefault();
    authnUsuario(emailInputLogin.value);
  });

  botonSubmitLogin.addEventListener("click", function (event) {
    event.preventDefault();
    authnUsuario(emailInputLogin.value);
  });
  async function authnUsuario(email) {
    try {
      const optionsResponse = await fetch("/iniciar_sesion", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      if (!optionsResponse.ok) {
        showAlertDanger("El correo no se encuentra registrado");
      }

      const options = await optionsResponse.json();
      options.challenge = urlBase64(options.challenge);
      options.allowCredentials.forEach(
        (cred) => (cred.id = urlBase64(cred.id))
      );

      const credential = await navigator.credentials.get({
        publicKey: options,
      });

      let sendCredential = credencialesPublicasAJson(credential);

      const verificarRepuesta = await fetch("/verifiar_inicio_sesion", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ response: sendCredential }),
      });

      if (!verificarRepuesta.ok) {
        throw new Error("Error al verificar la autenticación");
      }
      showAlertSuccess("Identificacion exitosa, recargando pagina!");
      document.getElementById("formularioLogin").reset();
      window.location.href = "/index.html";
    } catch (error) {
      console.error("Error con el servidor:", error);
    }
  }

  // Función para mostrar un mensaje de éxito
  function showAlertSuccess(message) {
    var messageContainer = document.getElementById("messageContainer");
    messageContainer.innerHTML = `
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              ${message}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`;
  }

  // Función para mostrar un mensaje de alerta
  function showAlertDanger(message) {
    var messageContainer = document.getElementById("messageContainer");
    messageContainer.innerHTML = `
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              ${message}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`;
  }

  let user;

  const loginLogoutButton = document.getElementById("login-logout-button");
  window.onload = function () {
    scrollChatToBottom();
  };

  function scrollChatToBottom() {
    var chatMessages = document.getElementById("chat-messages");
    chatMessages.scrollTop = chatMessages.scrollHeight;
  }

  document
    .getElementById("send-message")
    .addEventListener("click", enviarMensajeAlServidor);

  document
    .getElementById("user-message")
    .addEventListener("keypress", function (event) {
      if (event.key === "Enter") {
        enviarMensajeAlServidor();
      }
    });

  async function enviarMensajeAlServidor() {
    var chatContainer = document.getElementById("contenedor_chat");
    var message = document.getElementById("user-message").value;
    if (message) {
      // Enviar mensaje al backend
      document.getElementById("user-message").value = "";
      mensajeSistema("Procesando tu solicitud", "info");
      chatContainer.scrollTop = chatContainer.scrollHeight;
      sendMessageToBackend(message);
    }
  }

  async function sendMessageToBackend(message) {
    const url = "/chat/"; // Actualiza la URL para que coincida con la ruta en urls.py
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ message: message }),
      });

      if (!response.ok) {
        mensajeSistema("Hay un problema en el servidor..", "danger");
        throw new Error("Network response was not ok");
      }

      const responseData = await response.json();
      displayResponse(message, responseData.response);
    } catch (error) {
      console.error("Error:", error.message);
      // Mostrar mensaje de error al usuario
      displayResponse(
        message,
        "Error en el servidor. Por favor, inténtalo de nuevo más tarde."
      );
      mensajeSistema("Chatbot fuera de servicio..", "danger");
    }
  }

  async function saveMessageToDatabase(userMessage, botResponse) {
    try {
      if (user && user.user_id) {
        const data = {
          usuario: userMessage,
          MEDBot: botResponse,
          user_id: user.user_id, // Cambiar a user_id en lugar de user
        };

        const response = await fetch("/chat/save/", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        });

        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
      } else {
        throw new Error("No se pudo obtener el ID del usuario");
      }
    } catch (error) {
      console.error("Error saving message to database:", error.message);
    }
  }

  async function displayResponseHistory() {
    var chatContainer = document.getElementById("contenedor_chat");
    try {
      // Si no se puede obtener el ID del usuario, detener el proceso
      if (!user || !user.user_id) {
        console.error("No se pudo obtener el ID del usuario");
        return;
      }

      // Realizar la solicitud para obtener el historial de chat por ID de usuario
      const response = await fetch("/chat/history/?user_id=" + user.user_id, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const chatHistory = await response.json();

      if (chatHistory.length > 0) {
        // Ordenar los mensajes cronológicamente
        chatHistory.sort(
          (a, b) => new Date(a.timestamp) - new Date(b.timestamp)
        );
        for (let i = 0; i < chatHistory.length; i++) {
          const message = chatHistory[i];
          mostrarMensajeUsuario(message.user_message);
          mostrarRespuestaDeSistema(message.bot_response);
          chatContainer.scrollTop = chatContainer.scrollHeight;
        }
      } else {
        console.log("No hay historial de chat disponible");
      }
    } catch (error) {
      console.error("Error al recuperar el historial del chat:", error);
    }
  }

  // Función para mostrar el mensaje del usuario
  function mostrarMensajeUsuario(chatuser) {
    var chatMessages = document.getElementById("chat-messages");
    var userMessageElement = document.createElement("div");
    userMessageElement.classList.add("alert", "alert-success");
    userMessageElement.setAttribute("role", "alert");
    userMessageElement.textContent = chatuser;
    chatMessages.appendChild(userMessageElement);
  }

  // Función para mostrar la respuesta del sistema
  function mostrarRespuestaDeSistema(response) {
    var chatMessages = document.getElementById("chat-messages");
    var newMessage = document.createElement("div");
    newMessage.classList.add("alert", "alert-primary");
    newMessage.setAttribute("role", "alert");

    // Verificar si response es undefined o null antes de realizar el split
    if (response !== undefined && response !== null) {
      var responseLines = response.split("\n");
      responseLines.forEach(function (line) {
        var lineElement = document.createElement("div");
        lineElement.textContent = line;
        newMessage.appendChild(lineElement);
      });
    } else {
      // Si response es undefined o null, mostrar un mensaje de error o realizar otra acción necesaria
      console.error("La respuesta del sistema es indefinida o nula");
    }

    chatMessages.appendChild(newMessage);
    mensajeSistema("Esperando su solicitud", "success");
  }

  // Función principal que llama a las otras dos funciones según corresponda
  async function displayResponse(userMessage, botMessage) {
    mostrarMensajeUsuario(userMessage);
    mostrarRespuestaDeSistema(botMessage);
    yameteKudasai(botMessage);
    var chatContainer = document.getElementById("contenedor_chat");
    chatContainer.scrollTop = chatContainer.scrollHeight; // Desplazamiento automático cuando se llena de mensajes

    // Obtener el ID del usuario
    //user = await obtenerUserID();
    // Si el usuario está logueado, guardar los mensajes en la base de datos
    if (user && user.user_id) {
      saveMessageToDatabase(userMessage, botMessage);
    }
  }

  function mensajeSistema(mensaje, tipo) {
    var progressBar = document.getElementById("mensajes-sistema-constenido");
    progressBar.textContent = mensaje;

    // Limpiar clases de fondo y texto
    progressBar.classList.remove(
      "bg-primary",
      "bg-danger",
      "bg-secondary",
      "bg-success",
      "bg-info",
      "bg-warning",
      "text-dark"
    );

    // Establecer clase de fondo y texto según el tipo
    switch (tipo) {
      case "primary":
        progressBar.classList.add("bg-primary");
        break;
      case "danger":
        progressBar.classList.add("bg-danger");
        break;
      case "secondary":
        progressBar.classList.add("bg-secondary");
        break;
      case "success":
        progressBar.classList.add("bg-success");
        break;
      case "info":
        progressBar.classList.add("bg-info", "text-dark");
        break;
      case "warning":
        progressBar.classList.add("bg-warning", "text-dark");
        break;
      default:
        // Si el tipo no coincide con ninguno de los valores esperados, se usará el color por defecto
        progressBar.classList.add("bg-primary");
        break;
    }
  }

  function obtenerUserID() {
    fetch("/user_id", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          console.log("No se pudo obtener el ID del usuario");
        }
      })
      .then((data) => {
        user = data && data.user_id ? data : null;
        if (user) {
          // Si el usuario está autenticado
          displayResponseHistory();
          const offcanvasScrolling = new bootstrap.Offcanvas(
            document.getElementById("offcanvasScrolling")
          );
          offcanvasScrolling.show();
          loginLogoutButton.innerText = "Cerrar sesión";
          loginLogoutButton.classList.add("btn", "btn-primary");
          const icon = document.createElement("i");
          icon.classList.add("bi", "bi-box-arrow-left"); // Agrega un icono de cerrar sesión
          loginLogoutButton.prepend(icon);
          loginLogoutButton.addEventListener("click", async () => {
            // Realiza el cierre de sesión llamando a la ruta /logout
            await fetch("/logout", { method: "GET" });
            // Recarga la página para reflejar el estado de desconexión
            window.location.reload();
          });
        } else {
          // Si el usuario no está autenticado
          loginLogoutButton.innerText = "Iniciar sesión";
          loginLogoutButton.classList.add("btn", "btn-primary");
          const icon = document.createElement("i");
          icon.classList.add("bi", "bi-person"); // Agrega un icono de usuario
          loginLogoutButton.prepend(icon);
          loginLogoutButton.addEventListener("click", () => {
            // Abre el panel de registro de usuario
            const offcanvasScrollingID = new bootstrap.Offcanvas(
              document.getElementById("offcanvasScrollingID")
            );
            offcanvasScrollingID.show();
            const offcanvasScrolling = new bootstrap.Offcanvas(
              document.getElementById("offcanvasScrolling")
            );
            offcanvasScrolling.hide();
          });
        }
      })
      .catch((error) => {
        console.error("Error al obtener el ID del usuario:", error);
      });
  }

  console.log("Todos los elementos del DOM han sido cargados.");
  mensajeSistema("Esperando su solicitud", "success");
  obtenerUserID();

  //AGREGADOS EASTER EGG
  let contador = 0;

  function yameteKudasai(ingreso) {
    const texto = ingreso;
    // Aquí puedes modificar la palabra clave según tus necesidades
    const palabraClave = "MEDBot: Lo siento pero No comprendo...";
    if (texto.includes(palabraClave)) {
      contador++;
      if (contador == 10) {
        mostrarStickerDeSistema(yameteKudasaiGIF);
        reproducirSonido();
        contador = 1;
      }
    }
  }

  function reproducirSonido() {
    const audio = document.getElementById("audio");
    audio.play();
  }

  //Stikers
  // Función para mostrar la respuesta del sistema
  function mostrarStickerDeSistema(urlImagen) {
    var chatMessages = document.getElementById("chat-messages");
    var newMessage = document.createElement("div");
    newMessage.classList.add("alert", "alert-primary");
    newMessage.setAttribute("role", "alert");

    // Verificar si la URL de la imagen está definida
    if (urlImagen) {
        var imgElement = document.createElement("img");
        imgElement.src = urlImagen;
        imgElement.style.maxWidth = "100%"; // Establecer el ancho máximo de la imagen
        imgElement.style.height = "auto"; // Ajustar la altura automáticamente para mantener la proporción
        newMessage.appendChild(imgElement); // Agregar la imagen dentro del div
    } else {
        // Si la URL de la imagen no está definida, mostrar un mensaje de error o realizar otra acción necesaria
        console.error("La URL de la imagen no está definida");
    }

    chatMessages.appendChild(newMessage);
}

  var yameteKudasaiGIF = "../yameteKudasai.gif";
  let yamete = new Audio("yamete-kudasai.wav");
  //yamete.play();
  let uwu = new Audio("uwu.wav");
  //uwu.play();
});
