import { initializeApp } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-app.js";
import {
  getAuth,
  onAuthStateChanged,
  createUserWithEmailAndPassword,
  signOut,
} from "https://www.gstatic.com/firebasejs/10.1.0/firebase-auth.js";
import { getFirestore } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-firestore.js";
import {
  getDatabase,
  ref,
  set,
  child,
  get,
} from "https://www.gstatic.com/firebasejs/10.1.0/firebase-database.js";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

//Crear cuenta en Firebase//////////////////////////////////////////////////

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
//Firebase ////////////////////////////////////////////////////////////////
const firebaseConfig = {
  apiKey: "AIzaSyB0KQgbqajXPVmwSjNc_D7K8SSFXbquHPM",
  authDomain: "hosplarcade.firebaseapp.com",
  projectId: "hosplarcade",
  storageBucket: "hosplarcade.appspot.com",
  messagingSenderId: "542588355477",
  appId: "1:542588355477:web:669ef0ad1fe0e6beffac9c",
  measurementId: "G-7XP1JN7RM6",
  databaseURL: "https://hosplarcade-default-rtdb.firebaseio.com/",
};

// Initialize Firebase///////////////////////////////////////////////////////////
export const app = initializeApp(firebaseConfig);
export const database = getDatabase();
export const auth = getAuth();
export const db = getFirestore();

//REGISTRO DE USUARIO VAR GLOBALES/////////////////////////////////////////////
var campoNombre = document.getElementById("nombre");
var campoApellido = document.getElementById("apellido");
var campoDNI = document.getElementById("dni");
var campoEdad = document.getElementById("edad");
var campoTelefono = document.getElementById("telefono");
var campoDireccion = document.getElementById("direccion");
var campoCorreo = document.getElementById("correo");
var campoContraseña = document.getElementById("contraseña");
var campoConfirmarContraseña = document.getElementById("confirmar_contraseña");
var mensajeAviso = document.getElementById("mensajeAviso");
var alertaSistema = document.getElementById("alertaSistema");

var nombreBoolean = false;
var apellidoBoolean = false;
var dniBoolean = false;
var edadBoolean = false;
var telefonoBoolean = false;
var direccionBoolean = false;
var correoBoolean = false;
var contraseñaBoolean = false;
var confContraseñaBoolean = false;

var nombre;
var apellido;
var dni;
var edad;
var telefono;
var direccion;
var correo;
var contraseña;
var confirmarContraseña;

///VALIDACION DEL FORMULARIO////////////////////////////////////////////////////

// Acción cuando se envía el formulario de registro
document
  .getElementById("formulario")
  .addEventListener("submit", function (event) {
    event.preventDefault(); // Evitar el envío del formulario
    //Valida y registra
    validacionDeCampos();
  });

function validacionDeCampos() {
  if (
    nombreBoolean &&
    apellidoBoolean &&
    dniBoolean &&
    edadBoolean &&
    telefonoBoolean &&
    direccionBoolean &&
    correoBoolean &&
    contraseñaBoolean &&
    confContraseñaBoolean
  ) {
    mostrarAlerta(
      mensajeAviso,
      "Todo correcto!, enviando solicitud",
      "alert-success"
    );
    cargarCampos();
    registrarUsuario();
    return true;
  } else {
    mostrarAlerta(
      mensajeAviso,
      "Error: Algunos de los campos esta mal!, cancelando envio de solicitud",
      "alert-danger"
    );
    return false;
  }
}

function enviarAPagina() {
  window.location.href = "ingreso.html";
}

function cargarCampos() {
  nombre = campoNombre.value;
  apellido = campoApellido.value;
  dni = campoDNI.value;
  edad = campoEdad.value;
  telefono = campoTelefono.value;
  direccion = campoDireccion.value;
  correo = campoCorreo.value;
  contraseña = campoContraseña.value;
  confirmarContraseña = campoConfirmarContraseña.value;
}

var user;
var userId;

function registrarUsuario() {
  createUserWithEmailAndPassword(auth, correo, contraseña)
    .then((userCredential) => {
      // Signed in
      user = userCredential.user;
      console.log(user);
      onAuthStateChanged(auth, (user) => {
        if (user) {
          // User is signed in, see docs for a list of available properties
          // https://firebase.google.com/docs/reference/js/auth.user
          userId = user.uid;
          subirDatos();
          verificarDatos();
          // ...
        } else {
          // User is signed out
          // ...
        }
      });
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      if (errorCode == "auth/email-already-in-use") {
        mostrarAlertaSistema();
      }
      // ..
    });
}

function mostrarAlertaSistema() {
  cargarToast();
}

function subirDatos() {
  set(ref(database, "paciente/" + userId), {
    nombre: nombre,
    apellido: apellido,
    dni: dni,
    edad: edad,
    telefono: telefono,
    direccion: direccion,
    correo: correo,
  });
}

function verificarDatos() {
  const dbRef = ref(getDatabase());
  get(child(dbRef, `paciente/${userId}`))
    .then((snapshot) => {
      if (snapshot.exists()) {
        desconectarCuenta();
      } else {
        console.log("No data available");
      }
    })
    .catch((error) => {
      console.error(error);
    });
}

function desconectarCuenta() {
  signOut(auth)
    .then(() => {
      enviarAPagina();
    })
    .catch((error) => {
      // An error happened.
    });
}

//FORMULARIO
/// NOMBRE ////////////////////////////////////////////////////////////////////////////

campoNombre.addEventListener("input", function () {
  var valor = campoNombre.value.trim();
  // Validación del nombre
  validacionNombre(valor);
});

function validacionNombre(valor) {
  if (valor.length === 0 || valor.length > 50 || !/^[a-zA-Z]+$/.test(valor)) {
    mostrarAlerta(
      campoNombre,
      "Error: El nombre debe tener entre 1 y 50 caracteres y solo letras",
      "alert-danger"
    );
    nombreBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoNombre, "Nombre válido", "alert-success");
    nombreBoolean = true;
    return true;
  }
}

/// APELLIDO ////////////////////////////////////////////////////////////////////////////

campoApellido.addEventListener("input", function () {
  var valor = campoApellido.value.trim();
  // Validación del apellido
  validacionApellido(valor);
});

function validacionApellido(valor) {
  if (valor.length === 0 || valor.length > 50 || !/^[a-zA-Z]+$/.test(valor)) {
    mostrarAlerta(
      campoApellido,
      "Error: El apellido debe tener entre 1 y 50 caracteres y solo letras",
      "alert-danger"
    );
    apellidoBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoApellido, "Apellido válido", "alert-success");
    apellidoBoolean = true;
    return true;
  }
}

/// DNI //////////////////////////////////////////////////////////////////////////////

campoDNI.addEventListener("input", function () {
  var valor = campoDNI.value.trim();
  // Validación del apellido
  validacionDNI(valor);
});

function validacionDNI(valor) {
  if (valor.length === 0 || valor.length < 1) {
    mostrarAlerta(
      campoDNI,
      "Error: EL DNI debe ser al menos de 1000000",
      "alert-danger"
    );
    dniBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoDNI, "DNI válido", "alert-success");
    dniBoolean = true;
    return true;
  }
}

/// EDAD //////////////////////////////////////////////////////////////////////////////

campoEdad.addEventListener("input", function () {
  var valor = campoEdad.value.trim();
  // Validación del apellido
  validacionEdad(valor);
});

function validacionEdad(valor) {
  if (valor.length === 0 || valor <= 0 || valor > 120) {
    mostrarAlerta(
      campoEdad,
      "Error: La edad debe ser de entre 1 año y 120 años",
      "alert-danger"
    );
    edadBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoEdad, "Edad válida", "alert-success");
    edadBoolean = true;
    return true;
  }
}

/// TELEFONO //////////////////////////////////////////////////////////////////////////////

campoTelefono.addEventListener("input", function () {
  var valor = campoTelefono.value.trim();
  // Validación del apellido
  validacionTelefono(valor);
});

function validacionTelefono(valor) {
  if (valor.length === 0 || valor.length < 8 || !/^\d+$/.test(valor)) {
    mostrarAlerta(
      campoTelefono,
      "Error: El formato de teléfono es incorrecto",
      "alert-danger"
    );
    telefonoBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoTelefono, "Telefono válido", "alert-success");
    telefonoBoolean = true;
    return true;
  }
}

/// DIRECCION //////////////////////////////////////////////////////////////////////////////

let direccionValue;
let nuevoTexto;
var coordenadas;
var y;
var x;

var tituloDireccion = document.getElementById("tituloDireccion");
var address = document.getElementById("direccion");
var iconReference = document.querySelector(".input-group-text i");
var dropdownList = document.getElementById("myDropdown");

address.addEventListener("input", function () {
  var direccionValue = address.value;
  if (/\b\d+\b/.test(direccionValue)) {
    // Si el texto contiene un número, se mantiene sin modificar
    var nuevoTexto = direccionValue;
    var direccion = nuevoTexto;
    mostrarAlerta(
      tituloDireccion,
      "Error: debe ser de la forma 'CALLE NUMERO, CIUDAD' o 'CALLE, NUMERO, CIUDAD'",
      "alert-danger"
    );
  } else if (direccionValue.match(/([^,]+),\s*(.*)/)) {
    // Si el texto no contiene un número, separamos la primera parte y el resto
    var primeraParte = match[1].trim();
    var resto = match[2].trim();
    nuevoTexto = `${primeraParte} y ${resto}`;
    mostrarAlerta(
      tituloDireccion,
      "Error: debe ser de la forma 'CALLE NUMERO, CIUDAD' o 'CALLE, NUMERO, CIUDAD'",
      "alert-danger"
    );
  } else {
    nuevoTexto = direccionValue;
    mostrarAlerta(
      tituloDireccion,
      "Error: debe ser de la forma 'CALLE NUMERO, CIUDAD' o 'CALLE, NUMERO, CIUDAD'",
      "alert-danger"
    );
  }
  direccion = nuevoTexto;
  iconReference.classList.remove("fa", "fa-check-circle");
  iconReference.classList.add("fa", "fa-times-circle");
  direccionBoolean = false;

  if (direccion.trim() === "") {
    dropdownList.style.display = "none";
    return;
  }

  fetch(
    "http://servicios.usig.buenosaires.gob.ar/normalizar/?geocodificar=true&direccion=" +
      direccion
  )
    .then((response) => response.json())
    .then((data) => {
      dropdownList.innerHTML = "";
      data.direccionesNormalizadas.forEach((element) => {
        setElement(element);
      });
    });

  function setElement(element) {
    var listItem = document.createElement("li");
    listItem.id = "elementoListaDeDirecciones";
    listItem.classList.add("dropdown-item");
    listItem.style.cursor = "pointer";
    listItem.style.color = "#1688f2";
    listItem.style.fontWeight = "bold";
    listItem.style.backgroundColor = "#fff";
    listItem.style.border = "1px solid #ccc";
    listItem.style.marginTop = "5px";
    listItem.innerHTML =
      "<p>" +
      element.direccion +
      "<span class='badge bg-secondary' style='margin-left:10px;'>Seleccionar</span>" +
      "</p>";
    listItem.addEventListener("click", function () {
      address.value = element.direccion;
      if (
        address.value.match(/^[\w\sÁÉÍÓÚÑáéíóúñ.,]+ \d+,[\w\sÁÉÍÓÚÑáéíóúñ.,]+$/)
      ) {
        mostrarAlerta(tituloDireccion, "Formato correcto", "alert-success");
        direccionBoolean = true;
        iconReference.classList.remove("fa", "fa-times-circle");
        iconReference.classList.add("fa", "fa-check-circle");
      } else {
        mostrarAlerta(
          tituloDireccion,
          "Error: debe ser de la forma 'CALLE NUMERO, CIUDAD' o 'CALLE, NUMERO, CIUDAD'",
          "alert-danger"
        );
        direccionBoolean = false;
        iconReference.classList.remove("fa", "fa-check-circle");
        iconReference.classList.add("fa", "fa-times-circle");
      }
      dropdownList.style.display = "none";
    });
    dropdownList.appendChild(listItem);
  }

  dropdownList.style.display = "block";
});

document.addEventListener("click", function (event) {
  var targetElement = event.target;
  if (!targetElement.closest(".dropdown")) {
    dropdownList.style.display = "none";
  }
});

/// CORREO ////////////////////////////////////////////////////////////////////////////

campoCorreo.addEventListener("input", function () {
  var valor = campoCorreo.value.trim();
  // Validación del correo electrónico
  validacionCorreo(valor);
});

function validacionCorreo(valor) {
  if (!/^[\w.-]+@[\w.-]+\.\w+$/.test(valor)) {
    mostrarAlerta(
      campoCorreo,
      "Error: El correo electrónico no es válido, debe tener un formato correcto",
      "alert-danger"
    );
    correoBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoCorreo, "Correo electrónico válido", "alert-success");
    correoBoolean = true;
    return true;
  }
}
///CONTRASEÑA//////////////////////////////////////////////////////////////////////////

campoContraseña.addEventListener("input", function () {
  var valor = campoContraseña.value.trim();
  // Validación de la contraseña
  validacionContraseña(valor);
});

function validacionContraseña(valor) {
  if (valor.length < 8 || !/^[a-zA-Z0-9]+$/.test(valor)) {
    mostrarAlerta(
      campoContraseña,
      "Error: La contraseña debe tener al menos 8 caracteres alfanuméricos",
      "alert-danger"
    );
    contraseñaBoolean = false;
    return false;
  } else {
    mostrarAlerta(campoContraseña, "Contraseña válida", "alert-success");
    contraseñaBoolean = true;
    return true;
  }
}

///CONFIRMACION CONTRASEÑA//////////////////////////////////////////////////////////////////////////

campoConfirmarContraseña.addEventListener("input", function () {
  var valor = campoConfirmarContraseña.value.trim();
  var contraseña = campoContraseña.value.trim();
  // Validación de la confirmación de contraseña
  validacionConfirmacionContraseña(valor, contraseña);
});

function validacionConfirmacionContraseña(valor, contraseña) {
  if (valor !== contraseña) {
    mostrarAlerta(
      campoConfirmarContraseña,
      "Error: Las contraseñas no coinciden",
      "alert-danger"
    );
    confContraseñaBoolean = false;
    return false;
  } else {
    mostrarAlerta(
      campoConfirmarContraseña,
      "Contraseña confirmada",
      "alert-success"
    );
    confContraseñaBoolean = true;
    return true;
  }
}

//Funciones adicionales/////////////////////////////////////////////////////////////////////////////////////////////

function esperarAElemento(idElemento) {
  console.log("inicio funcion esperarAElemento");
  var intervalo = setInterval(function () {
    var elemento = document.getElementById(idElemento);
    if (elemento) {
      console.log("aparecio el elemento");
      clearInterval(intervalo);
      buscarYPonerImagen();
      return true;
    }
  }, 100); // Comprobar cada 100ms
}

function checkComas(text, numCommas) {
  var arr = text.split(",");
  return arr.length - 1 === numCommas;
}

function esNumero(str) {
  return !isNaN(str);
}

var alertTimers = new Map(); // Usamos un Map en lugar de un objeto

function mostrarAlerta(campo, mensaje, clase) {
  var divAlerta = campo.parentElement.querySelector(".alert");
  if (divAlerta) {
    campo.parentElement.removeChild(divAlerta);
  }

  var divAlertaNueva = document.createElement("div");
  divAlertaNueva.classList.add("alert", clase);
  divAlertaNueva.setAttribute("role", "alert");
  divAlertaNueva.textContent = mensaje;
  campo.parentElement.appendChild(divAlertaNueva);

  // Cancelar temporizador existente si hay uno
  if (alertTimers.has(campo)) {
    clearTimeout(alertTimers.get(campo));
  }

  // Configurar el temporizador para eliminar la alerta después de 2000 ms
  var timer = setTimeout(function () {
    if (campo.parentElement.contains(divAlertaNueva)) {
      campo.parentElement.removeChild(divAlertaNueva);
    }
    alertTimers.delete(campo); // Eliminar la referencia al temporizador
  }, 2000);

  alertTimers.set(campo, timer); // Guardar el temporizador en el Map
}

///Alerta de sistema
function cargarToast() {
  var toastBootstrap = bootstrap.Toast.getOrCreateInstance(alertaSistema);
  toastBootstrap.show();

  // Calcular la posición vertical para centrar el toast con un margen superior
  var windowHeight = window.innerHeight;
  var toastHeight = alertaSistema.offsetHeight;
  var marginTop = (windowHeight - toastHeight) / 2;
  if (marginTop > 20) {
    // Asegurarse de que haya un margen mínimo de 20px
    alertaSistema.style.marginTop = marginTop + "px";
  }

  // Desplazar la vista del usuario hasta el toast
  alertaSistema.scrollIntoView({ behavior: "smooth", block: "center" });
}
