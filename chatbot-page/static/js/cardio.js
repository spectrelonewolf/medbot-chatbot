document.addEventListener("DOMContentLoaded", function () {
    // variable global
  
    //Icono personalizado del mapa
    var iconoDestacado = L.icon({
      iconUrl: "./image/iconoMapa/destacado.png",
      shadowUrl: "./image/iconoMapa/destacadoSombra.png",
  
      iconSize: [50, 50], // size of the icon
      shadowSize: [70, 70], // size of the shadow
      iconAnchor: [40, 50], // point of the icon which will correspond to marker's location
      shadowAnchor: [45, 60], // the same for the shadow
      popupAnchor: [-15, -40], // point from which the popup should open relative to the iconAnchor
    });
  
    var latitud;
    var longitud;
    var map;
    function mostrarUbicacion() {
      latitud = -34.535966;
      longitud = -58.720583;
      map = L.map("map").setView([latitud, longitud], 17);
      L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(map);
  
      marcadorinicial = L.marker([latitud, longitud], { icon: iconoDestacado })
        .addTo(map)
        .bindPopup("Consultorios, Hosp. Dr. Raúl F. Larcade")
        .openPopup();
    }
  
    mostrarUbicacion();
  });