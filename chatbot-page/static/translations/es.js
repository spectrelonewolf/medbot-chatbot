const es = {
  "authentication": {
    "signup": {
      "start": {
        "header": "Crear tu cuenta",
        "subheader": "¿Ya tienes una cuenta?",
        "button_login": "Iniciar sesión",
        "button_submit": "Continuar",
        "textField_name": "Nombre",
        "textField_email": "Correo electrónico"
      },
      "emailOtp": {
        "header": "Ingresa el código de un solo uso para crear una cuenta",
        "body_text1": "Acabamos de enviar un código de un solo uso a ",
        "body_text2": ". El código de un solo uso expira pronto, así que ingrésalo pronto.",
        "button_verify": "Continuar",
        "button_sendOtpAgain": "Enviar código de un solo uso nuevamente",
        "button_back": "Cancelar"
      },
      "emailLinkSent": {
        "header": "Revisa tu bandeja de entrada para crear tu cuenta",
        "body_text1": "Acabamos de enviarte un enlace mágico a ",
        "body_text2": ". Haz clic en el enlace del correo electrónico para terminar de configurar tu cuenta.",
        "button_sendLinkAgainWaitingText": "Enviar correo electrónico nuevamente ({{remainingTime}} seg)",
        "button_sendLinkAgain": "Enviar correo electrónico nuevamente",
        "button_back": "Cancelar"
      },
      "emailLinkVerification": {
        "header": "Verificando tu dirección de correo electrónico",
        "button_back": "Cancelar"
      },
      "passkeyCreate": {
        "header": "Vamos a configurarte con ",
        "headerButton_showPasskeyBenefits": "Claves de acceso",
        "body": "Crearemos una cuenta para ",
        "button_start": "Crear tu cuenta",
        "button_switchToAlternate": {
          "emailOtp": "Enviar código de un solo uso por correo electrónico",
          "emailLink": "Enviar enlace mágico por correo electrónico"
        },
        "button_back": "Regresar"
      },
      "passkeyError": {
        "header": "Vamos a configurarte",
        "body_errorMessage": "No fue posible crear tu cuenta con ",
        "button_showPasskeyBenefits": "claves de acceso",
        "body_tryAgainMessage": {
          "emailOtp": ". Inténtalo de nuevo o regístrate con un código de un solo uso por correo electrónico.",
          "emailLink": ". Inténtalo de nuevo o regístrate con un enlace mágico por correo electrónico."
        },
        "button_switchToAlternate": {
          "emailOtp": "Enviar código de un solo uso por correo electrónico",
          "emailLink": "Enviar enlace mágico por correo electrónico"
        },
        "button_retry": "Intentar de nuevo",
        "button_back": "Regresar",
        "button_cancel": "Cancelar"
      },
      "passkeySuccess": {
        "header": "¡Bienvenido!",
        "subheader": "Clave de acceso creada",
        "body_text1": "Ahora puedes confirmar tu identidad usando tu ",
        "body_text2": {
          "emailOtp": "clave de acceso o mediante un código de un solo uso por correo electrónico",
          "emailLink": "clave de acceso o mediante un enlace mágico por correo electrónico"
        },
        "body_text3": " cuando inicies sesión.",
        "button": "Continuar"
      },
      "passkeyBenefits": {
        "header": "Claves de acceso",
        "body_introduction": "Con las claves de acceso, ya no necesitas recordar contraseñas complejas. Inicia sesión de forma segura utilizando ",
        "body_loginMethods": "Face ID, Touch ID o código de bloqueo de pantalla.",
        "button_start": "Crear clave de acceso",
        "button_skip": "Quizás después"
      },
      "passkeyAppend": {
        "header": "Inicia sesión aún más rápido con ",
        "button_showPasskeyBenefits": "Claves de acceso",
        "button_start": "Activar",
        "button_skip": "Quizás después"
      }
    },
    "login": {
      "start": {
        "header": "¡Bienvenido de nuevo!",
        "subheader": "¿Todavía no tienes una cuenta?",
        "button_signup": "Crear cuenta",
        "button_submit": "Continuar",
        "textField_email": "Dirección de correo electrónico"
      },
      "emailOtp": {
        "header": "Ingresa el código de un solo uso para iniciar sesión",
        "body_text1": "Acabamos de enviar un código de un solo uso a ",
        "body_text2": ". El código de un solo uso expira pronto, así que ingrésalo pronto.",
        "button_verify": "Continuar",
        "button_sendOtpAgain": "Enviar código de un solo uso nuevamente",
        "button_back": "Cancelar"
      },
      "emailLinkSent": {
        "header": "Revisa tu bandeja de entrada para iniciar sesión",
        "body_text1": "Acabamos de enviarte un enlace mágico a ",
        "body_text2": ". Haz clic en el enlace del correo electrónico para iniciar sesión en tu cuenta.",
        "button_sendLinkAgainWaitingText": "Enviar correo electrónico nuevamente ({{remainingTime}} seg)",
        "button_sendLinkAgain": "Enviar correo electrónico nuevamente",
        "button_back": "Cancelar"
      },
      "emailLinkVerification": {
        "header": "Verificando tu dirección de correo electrónico",
        "button_back": "Cancelar"
      },
      "passkeyError": {
        "header": "Inicia sesión con claves de acceso",
        "body": {
          "emailOtp": "No es posible iniciar sesión con claves de acceso. Inténtalo de nuevo o inicia sesión con un código de un solo uso por correo electrónico.",
          "emailLink": "No es posible iniciar sesión con claves de acceso. Inténtalo de nuevo o inicia sesión con un enlace mágico por correo electrónico."
        },
        "button_switchToAlternate": {
          "emailOtp": "Enviar código de un solo uso por correo electrónico",
          "emailLink": "Enviar enlace mágico por correo electrónico"
        },
        "button_retry": "Intentar de nuevo",
        "button_back": "Regresar",
        "button_cancel": "Cancelar"
      },
      "passkeyAppend": {
        "header": "Inicia sesión aún más rápido con ",
        "button_showPasskeyBenefits": "Claves de acceso",
        "button_start": "Activar",
        "button_skip": "Quizás después"
      },
      "passkeyBenefits": {
        "header": "Claves de acceso",
        "body_introduction": "Con las claves de acceso, ya no necesitas recordar contraseñas complejas. Inicia sesión de forma segura utilizando ",
        "body_loginMethods": "Face ID, Touch ID o código de bloqueo de pantalla.",
        "button_start": "Crear clave de acceso",
        "button_skip": "Quizás después"
      }
    },
    "unexpectedError": {
      "header": "Algo salió mal",
      "subheader": "Lamentamos que nuestro servicio no esté disponible actualmente.",
      "body_withCustomerSupport": "Inténtalo de nuevo en unos momentos y si el problema persiste, por favor contacta a {{customerSupportEmail}}.",
      "body_noCustomerSupport": "Inténtalo de nuevo en unos momentos.",
      "button": "Actualizar página"
    }
  },
  "errors": {
    "invalidName": "Por favor, ingresa un correo electrónico válido",
    "invalidFullname": "Por favor, ingresa un nombre válido",
    "passkeyChallengeCancelled": "Desafío de clave de acceso cancelado",
    "userAlreadyExists": "La dirección de correo electrónico ya está en uso. Si esta cuenta es tuya, inicia sesión en su lugar.",
    "passkeyAlreadyExists": "Ya existe una clave de acceso para este dispositivo",
    "unknownUser": "El usuario no existe",
    "noPasskeyAvailable": "No hay una clave de acceso disponible",
    "invalidPasskey": "La clave de acceso proporcionada ya no es válida. Inicia sesión usando tu dirección de correo electrónico en su lugar.",
    "invalidOtp": "El código de un solo uso proporcionado no es válido",
    "invalidToken": "El token proporcionado no es válido para la verificación del usuario",
    "noConditionalUiSupport": "La IU condicional no es compatible con tu dispositivo",
    "conditionalUiUnconfirmedCredential": "Esta clave de acceso aún no puede ser utilizada. Por favor, confirma tu identidad primero iniciando sesión con tu dirección de correo electrónico.",
    "unknownError": "Algo salió mal. Por favor, inténtalo de nuevo más tarde"
  },
  "passkeysList": {
    "warning_notLoggedIn": "Por favor, inicia sesión para ver tus claves de acceso.",
    "message_noPasskeys": "Aún no tienes ninguna clave de acceso.",
    "button_createPasskey": "Crear una clave de acceso",
    "badge_synced": "Sincronizado",
    "field_credentialId": "ID de credencial: ",
    "field_created": "Creado: {{date}} con {{browser}} en {{os}}",
    "field_lastUsed": "Último uso: ",
    "field_status": "Estado: ",
    "dialog_delete": {
      "header": "Eliminar clave de acceso",
      "body": "¿Estás seguro de que deseas eliminar esta clave de acceso?",
      "button_cancel": "Cancelar",
      "button_confirm": "Sí, eliminar"
    },
    "dialog_passkeyAlreadyExists": {
      "header": "La clave de acceso ya existe",
      "body": "Ya existe una clave de acceso para este dispositivo. Si tienes problemas con tu clave de acceso, por favor elimínala y crea una nueva.",
      "button_confirm": "Aceptar"
    }
  }
};
export default es;